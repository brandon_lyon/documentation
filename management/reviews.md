# Review Template
1. INTRO
    1. Be mindful of time
    1. Set the tone and outline the meeting expectations
    1. Make sure everyone is comfortable
    1. Lead with compliments and things done well
1. ACCOMPLISHMENTS
    1. List
    1. Ask if there’s anything they’d like to add & take notes
1. COMPENSATION
    1. Be detailed
1. OPPORTUNITIES FOR IMPROVEMENT
    1. Try to keep it brief and focused
    1. Ask for questions or comments
1. GOALS
    1. Discuss suggested goals
    1. Ask how the suggested goals feel
    1. Ask what goals they have & take notes
    1. Ask what we can do to help with those goals
1. OUTRO
    1. Ask if they have any questions
    1. Ask if they have any feedback
