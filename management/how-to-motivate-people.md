# How to Motivate People
1. Meet their hierarchy of needs (see Maslow).
1. Everyone is motiviated by different things. Get to know your team.
1. It can be demotivating if the reward for a job well done is the same as the reward for a job poorly done.
1. Below are some general tips:
---
1. Encourage safe communication. If someone is unhappy, you want to know that.
1. Be an advocate for their needs, especially since others might not be aware of their impact.
1. A burned out team member is not be productive.
    1. Do whatever you can to decrease their stress.
    1. Make sure they're getting enough rest and relaxation. Encourage time off.
    1. Suggest they take breaks throughout the day, perhaps meditate, play with pets, or go outside and walk around for a bit.
    1. Review their workload to make sure it's appropriate.
1. A team member will be most productive when they're working on something they enjoy that challenges them.
    1. Assigning people the right projects and allowing them to pick projects are both key to this.
    1. Sometimes a task needs to get done that they don't want to do. At least try to make it fun for them, get it over with quickly, show them appreciation, and give them first dibs on something more preferable next time.
    1. Ask people where they want to go in their career and what projects they want to do. Help them achieve those goals.
1. A team member is unlikely to be productive without appreciation.
    1. Why put in extra effort if it feels like nobody cares nor appreciates you or your work?
1. Ensure that directions are agreed upon, well defined, and prevent moving targets.
    1. It's hard to be productive if goals change or you don't know where you're going.
1. Everyone should be set up for success. Set realistic goals. Infeasible goals cause stress.