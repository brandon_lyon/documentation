# Management Tips and Tricks

## Productivity

1. A team member cannot be productive until their Maslow's hierarchy of needs are met.
    1. Encourage safe communication. If someone is unhappy, you want to know that.
    1. Be an advocate for their needs to stakeholders and leadership.
1. Everyone is motivated by different things.
    1. Do they want fame? Fortune? To help the environment? To have fun experiences?
    1. Some people enjoy a party, others want to be left alone with a good book.
    1. Some people are motivated by food, some aren't.
        1. If motivated by food, everyone likes different kinds of food.
1. A burned out team member will not be productive.
    1. Do whatever you can to decrease their stress.
    1. Make sure they're getting enough rest. Encourage time off.
    1. Suggest they take breaks throughout the day, perhaps meditate, play with pets, or go outside and walk around for a bit.
    1. Review their workload to ensure it's appropriate.
    1. Don't juggle too many tasks. Too many things to think about means less time to think about each thing.
1. A team member will be most productive when they're working on something they enjoy.
    1. Assigning people the right projects and allowing them to pick projects are both key to this.
        1. Sometimes a task needs to get done that they don't want to do. At least try to make it fun for them, get it over with quickly, show them appreciation, and give them first dibs on something more preferable next time.
    1. Ask people where they want to go in their career and what projects they want to do. Help them achieve those goals.
1. A team member is unlikely to be productive without appreciation.
    1. Why put in extra effort if it feels like nobody cares nor appreciates you or your work?
1. Ensure directions are agreed upon, well defined, and not moving targets.
    1. It's hard to be productive if goals change or you don't know where you're going.
1. Set realistic goals. Infeasible goals cause stress. Everyone should be set up for success.
1. Protect their time. 
    1. Meetings should not take up more than 50% of a week. Ideally, significantly less than that.
    1. As a team, agree on a protected schedule. For example, meetings can only be scheduled during certain hours or days...
        1. In the first half of a day, during preselected hours such as no earler than 7AM Pacific and no later than 1PM Eastern.
        1. In the first half of a week, so that weekly planning can happen and deliverables can be prepared for a Monday release.
