# Website project size calculator and lead times

## Minimum lead times

1. More lead time is always better.
1. The calculator does not include time for copy iteration nor design. It is only for estimating implementation time.
1. The size of a project determines how much advanced notice we need.
1. The bare minimum requested is 1 week lead time. Same-week requests are difficult to deliver on-time.
1. If a project is needed sooner than the calculator suggests, it might be a good idea to reduce scope.
1. These are not timelines for delivery. These are estimates for how long a project might take to implement, assuming no other projects are in front of it in the queue.

Emergencies can skip required lead times if they're truly an emergency.

[What is an emergency?](#)

## Project size vs lead time

| Size | Lead time | Points |
|------|-----------|--------|
| XS   | 1 week    | 1
| SM   | 1.5 weeks | 2
| MD   | 2 weeks   | 4
| LG   | 3 weeks   | 8
| XL   | 1 months  | 12
| 2XL  | 2+ months | 16

## Project size calculator

*Please reference this page before every project. This is likely to be iterated upon.*

1. The total points corresponds to the project difficulty.
1. Notice there are no point values when reusing assets. Reuse is encouraged for velocity.

* +1 for content adjustments on an existing page, without any changes to layout or structure.
* +1 for a new page or post from a preexisting template
* +1 for adding and testing a new (not preexisting) form to a page on a template that supports forms.
* +1 for adding form support to an unsupported page.
* +1 for new analytics events
* +1 for a new analytics dashboard
* +1 for a new simple component
* +3 for a new complex component
* +5 for a new layout template
* +1 for a simple animation
* +3 for a complex animation

## Common examples

> Implement a new blog post `+1(1)`.

0 + 1(1) = `1`

> Implement a new page from an existing template `(1)`.

0 + 1(1) = `1`

> Build a new landing page from an existing template `1(1)` which supports forms `0(1)` using an existing form `1(1)`.

0 + 1(1) + 0(1) + 0(1) = `1`

> Add a new form `1(1)` to a new page using an existing template `1(1)` which doesn't support forms `1(1)`.

0 + 1(1) + 1(1) + 1(1) = `3`

> Implement a new page template `5(1)` using 2 old components `0(2)`, 2 new simple components `1(2)`, and 1 new complex component `3(1)`.

0 + 0(1) + 5(1) + 1(2) + 3(1) = `10`

## FAQ

1. What are examples of complex components?
    1. Tabbed interfaces, sliders, media players, surveys, scroll jackers, fixed position elements, etc.
1. What are examples of simple system integrations?
    1. We add a javascript into our Tag Manager with minimal to no testing needed.
1. What are examples of large system integrations?
    1. We have to get infrastructure to set up something, we need to map events or flows, or write supporting code.