# Goals

* Example: increase organic conversion rate.
* Insert list items here

## Proposed methodology

Example: Based on previous discussions I imagine starting with one of the 3 main value-drivers linked from the homepage: [speed](https://about.gitlab.com/just-commit/reduce-cycle-time/).

`Insert methodology here`

In scope:

* Example: Layout
* Insert list items here

Out of scope:

* Example: content changes
* Insert list items here

#### Stakeholders

* List who and why
* Insert list items here

#### Deliverables, data gathering phase

- [x] Example: Obtain before screenshots
- [x] Example: Begin heatmap test for baseline comparison

#### Deliverables, data assessment phase

- [x] Example: Evaluate heatmap results from baseline to inform data-driven design decisions

#### Deliverables, design phase

Example: Prototype and/or wireframe containing:

- [x] Example: 1 desktop view before submit
- [x] Example: 1 desktop view after submit
- [x] Example: 1 mobile view
- [x] Example: Obtain review approval from prototype

#### Deliverables, engineering phase

- [x] Example: Build updated pages
- [x] Example: Obtain review approval from review-app
- [x] Example: End previous heatmap
- [x] Example: Release updated pages
- [x] Example: Create new heatmap

#### Deliverables, monitor phase

- [ ] Example: Monitor statistics
- [ ] Example: Report results

## Regression Risks

- [x] Example: Do the gated content forms still work?
- [x] Example: Does cookie consent still work in CCR & GDPR?
- [x] Example: Does the form still present GDPR & Ukraine required fields?
