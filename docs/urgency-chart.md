
# Urgency Levels

* Rushed projects lead to suboptimal results.
* No projects will be scheduled without finalized content.
* Please consider how your actions impact others.

Below are our urgency levels from the perspective of our engineering team.

1. The website is down, PII is being exposed, or some equally serious emergency needs to be taken care of.
    1. We plan to work on this ASAP. As a result, **the release of other projects WILL be delayed**.
2. An unforeseen time-sensitive request needs to be done ASAP **AND** will require no more than 50 minutes of implementation time.
    1. Overtime is likely and results might be suboptimal.
3. A project was requested without enough time, but we still want *something* before a hard deadline.
    1. Either this project gets reduced in scope or its release doesn’t meet the deadline.
4. An unexpected project was requested for completion within the active cycle.
    1. In order to compensate for reduced capacity, other projects might be delayed until after this cycle or reduced in scope.
5. A project was requested through our normal planning process with sufficient lead time.
    1. Everyone is happy.
