# Project Management Best Practices

## Overview

1. All projects start out as requests.
    * The team needs to agree on details, priorities, and capacity before accepting the request and scheduling it for implementation.
1. Project estimates and timeframes should not be given until we have copy.
    * The copy does not have to be final, but it does have to be releasable as-is. We can iterate afterward as needed.
1. Project requests will not be actionable without:
    1. A filled out project request form.
    1. Sufficient advance notice.
        * If a design is needed, increase the amount of advanced notice. 
        * If possible, please submit your requests one quarter in advance, to facilitate planning.

## Backlog

There is always more to do and we can't get it all done. We should be selective. A light backlog is a happy backlog.

## Focus on the Predictable

Try to keep no more than 1 or 2 active tasks in our queue at a time per person. If we try to plan too far in advance, then we're likely to burn out.

## Prioritize the Actionable

Requests should be actionable. If a project request doesn't have sufficient details or advance OKRs, it's okay to deny the request until those problems are alleviated.

## Time Management Math

Estimate capacity based on people-hours per quarter, and work your way back from there. Always leave buffer space, and remember to account for time off. 1 point is a small task, 2 points medium, 4 points lg, 8 points xl, 16 xxl, etc

* Plan for half your week to be administrative. There are always meetings, planning sessions, and unexpected events.
* The smallest unit of work is half a day. Everything has overhead. Focus switching, spin-up time, wind-down time, communication, documentation, verification, and more.
* We need to leave 20% buffer room, typically at the end of the week. This is for tasks outside of our planned workload. Examples include emergency projects or small quality of life projects.
* This means we have 4 points of work per week per person.
* There are roughly 4 weeks in a month, and 3 months per quarter.
* We need to account for holidays and other time off.
* 4 points per week, 4 weeks per month, 3 months per quarter
* 4 * 4 * 3 = 48 points per quarter, MINUS any time off.
* Assuming 12 days PTO plus 12 calendar holidays per year, that means we lose 6 days per quarter.
* 48 - 12 = 

**36 points of capacity per person per quarter**

* Assuming each project takes on average 2 days to complete, this means on average...

**9 planned projects max per person per quarter, or roughly 3 per month.**

* Some projects are longer, some are shorter. Some quarters have more room, some have less.
* At least 1 of those projects is internally sourced by the implementing team. This often includes cleaning up debt or integrating new technologies.

## FAQ

1. Why can't you work on a small request right away?
    1. We get multiple small requests every cycle. If we do unplanned work, then we cannot deliver the work we planned.
    1. There is overhead in context switching, creating work logs, merge/pull requests, branches, automation, documentation, reviewing (code, copy, UI, UX, AX, CX, DIB, SEM, QA, social), and verifying releases.
    1. If you have an emergency then please let us know, but be mindful that such requests negatively impact other work.

## Project Stages

| Stage | Description |
|---|---|
| **New** | New requests go here. There is no promise of delivery. |
| **Refine** | We will be unable to work on this until the request has been clarified. |
| **To Do** | This issue is ready to be worked on and has been assigned a priority relative to other tasks in the backlog. |
| **Doing** | This is actively being worked on. |
| **Blocked** | Despite having assigned resources, something is preventing us from working on this. |
| **In Review** | Before we release anything, it needs to be reviewed by someone other than the contributor. No exceptions. Ideally it would be reviewed by a designated stakeholder and QA. |

# Scheduling Priorities

== **Please consider how your requests impact others. ==**

- Rushed projects lead to suboptimal results.
- If content is needed, then the project cannot be scheduled without finalized content.
- Increasing scope is likely to delay releases.

Below are our urgency levels from the perspective of our engineering team.

1. The website is down, PII is being exposed, or some equally serious emergency needs to be taken care of.
    * We plan to work on this ASAP. As a result, **the release of other projects WILL be delayed**.
2. An unforeseen time-sensitive request needs to be done ASAP **AND** will require no more than 50 minutes of implementation time.
3. This project cannot wait for our normal planning cycle.
    * This will negatively impact other projects including our OKRs, workload, and expected progress.
4. A project was requested through our normal planning process with sufficient lead time.
    * Everyone is happy.
5. There is insufficient business need for action on this issue, but there is sufficient desire to keep this issue in the backlog.
