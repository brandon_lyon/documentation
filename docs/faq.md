# Digital FAQ

<details markdown="1">

<summary>Why can't you work on a small request right away?</summary>

### Why can't you work on a small request right away?

Unfortunately we get multiple of these requests every day. When we do unplanned work, then we cannot deliver all of the work we planned. That said, it doesn't hurt to ask. We might have availability.

If we don't have availability, then we respectfully ask for your patience while waiting for the next available [{{TBD}} agency day](#). This is time we set aside for this kind of work.

Your request might only take a few minutes to implement, but there is additional overhead in context switching, creating issues + merge requests + branches, testing procedures, reviewing, and verifying releases.

If you have an emergency then please do let us know. Be aware that whenever new work is brought into a sprint, other work is negatively impacted. Your request might be important, but we still need to weigh the importance against other projects.

</details>

<details markdown="1">

<summary>Why is my issue marked not-actionable?</summary>

### Why is my issue marked not-actionable?

This is a specific type of blocked issue. An issue is not actionable if our team is missing what we need to work on it. We might have unanswered questions or the project might be missing a content plan.

</details>

<details markdown="1">

<summary>What logos do we have permission to use?</summary>

### What logos do we have permission to use?

On our website we have approval to use the customer logos lisited at the following link, [{{TBD}} Approved customer logos for promotion](#)

</details>

<details markdown="1">

<summary>Can I request a specific project release time?</summary>

### Can I request a release time?

* Due to how CI/CD deployment pipelines work, release times will NOT be exact. Please plan accordingly. We try to have releases live within 1 hour of the requested timeframe.
* When requesting a release time, please specify a timezone.
* Reasons we can't guarantee a release time include:
  * We don't have a dynamic server, all items are pre-compiled and static.
  * Pipelines might have hundreds of people already in the queue before you.
  * Pipelines might be broken.
  * It takes an unknown amount of time for pipelines to allocate resources, build, run tests, and deploy.
  * It takes time for our CDN to propagate any changes across their network.
  * We don't have dedicated QA resources to ensure that things will happen as expected.
* If planning to release AT a specific time...
  * Request a time an hour before the expected release.
  * Plan for your item to appear before the expected release time. This might mean supplying alternate visuals or copy.
* If planning to release AFTER a specific time...
  * Plan for the preexisting content to cover that time range.
* Please ensure requested times are during normal business hours for the person making the changes.
  * If any changes are requested outside of their normal business hours, please ask before hand if that is possible or if someone else who is available can work on it to ensure that it releases in a timely fashion.

</details>

<details markdown="1">

<summary>Why isn't this form working?</summary>

### Why isn't this form working?

Many of our forms are served through a third party tool such as Hubspot or Marketo. Sometimes these tools are blocked by ad blockers or privacy strict web browser such as [Brave](https://brave.com/). GDPR and CCPA (among other laws) require us to obtain consent before setting cookies and those cookies are required for many third party functions.

If you are having trouble using a form, please try <a href="javascript:Cookiebot.renew();">updating your cookie settings</a> to allow **personalization (personal information) cookies**. If that does not work, please try a different browser with a less strict adblocker and ensure our domain and subdomains {{DOMAINS_LIST}} are not blocked.

If you have tried the above solutions but are still having trouble using a form, please [{{TBD}} file a bug report](#). Please note that if you do not provide all of the requested information we might be unable to reproduce the bug and therefore unable to fix it.

</details>

<details markdown="1">

<summary>Why can't I see something in the review apps?</summary>

### Why can't I see something in the review apps?

This is potentially due to a cookie consent platform. Because of how our infrastructure is setup, certain third party content (such as youtube embeds) or advanced javascript may not appear on review apps. If you are unsure of the cause, please [{{TBD}} contact us](#) so we can help review what might be impacting your project.

</details>
