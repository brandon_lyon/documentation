Either for us or for our users, could this issue…

1. Impact security?
2. Prevent customers from accessing their account?
3. Cause a service disruption?
4. Cause data loss?
5. Cause revenue loss?
6. Prevent signups?
7. Impact legal obligations?
8. Damage reputation and/or trust?