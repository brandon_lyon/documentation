
# Analytics best practices

## Purpose of dashboards

A good dashboard exists to answer a singular specific question.

- Don't stuff everything on the same dashboard.
- Bad: What are the core website metrics?
    - Good: How is overall traffic doing?
- Bad: How well is this page converting?
    - Good: How does this page impact signups?
- Bad: How many downloads do we have overall?
    - Good: How often do visitors download?
    - Good: Which downloads are enticing?
- Bad: Where does our traffic come from?
    - Good: What is a typical visitor?
- Bad: How many pageviews, sessions, etc?
    - Good: How long are they on the site?
    - Good: How often do they visit the site?
- Bad: Did they find what they were looking for?
    - Good: Are navigation items being used?
    - Good: What are people searching for & do we have that content?

## Events

- Before creating an event, first create a bucket where you can combine multiple events. This makes it easier to track events as a page changes.

## Time periods

- All time periods can have recurring impacts.
    - Most deals close at the end of a quarter.
    - Many websites get fewer visitors on weekends.
- Time periods should default to 30 days for most projects.
    - UX needs sufficent time to accumulate data.
    - 30 days helps account for start vs end of month trends.
    - If it takes 90 days to notice a problem, that could be bad.
- 7 days can help you see the short term impacts of campaigns or launches, often useful for digital marketing.
- Time periods shorter than 7 days do not usually contain actionable data.
- 90 days is useful to track where quarterly OKRs are trending.
- Annual can help with long term plans.

## Specificity

- You can reuse events by applying filters. For example, a generic event of "unique page visits over 90 days" could be filtered to a specific page in a report.
- If you want to exclude staging and test environments, apply domain equals primary to all reports.
- If you want to filter to a specific page, use path equals. URL parameters should not impact this.
- If you want to filter to a specific folder, use path contains. This might be used during multi-page campaigns.
    - For example to measure a component used across an aggregate of /learn/, /learn/argo/, and /learn/gitops/, the filter would be path contains /learn/.

## Naming conventions

- Any view in a dashboard should be self evident. We shouldn't have to follow a trail to find out what it's for.
- Include who, where, how, what, and when in the name.
    - Bad examples
        - Enterprise
        - Pricing
        - Features
    - Good examples
        - Marketing > Pages > Pricing > Clicked buy enterprise > Sessions 30 days
        - Marketing > Pages > Pricing > Submitted any form > Unique 30 days
        - Marketing > Pages > Pricing > Viewed feature table > All 90 days
