**This document is https://creativecommons.org/licenses/by/3.0/ by Brandon M Lyon**

---

# How to write documentation

The purpose of this page is to document best practices related to writing documentation.

* **They** = consumers.
* **You** = publishers.

## General writing tips

* User attention span is short. Be succinct when asking a question and ask as few questions as possible.
* Always include an introduction and a conclusion.
  * The introduction should list what it's for, what it's not for, and who might read it.
    * Example: *This document is about known bugs on codefresh.io and is reviewed by the Growth Marketing team. This document is not for the Codefresh product.*
  * The conclusion should include a checklist reviewing the actions they're required to do.
    * Example: *All sections have been filled out, relevant documents have been attached, a due date was specified with reason, all copy is finalized.*

## Choose a purpose

Not all content is applicable in all situations. The first step is to choose what topic the issue is about. Don't create one document for everything, create a separate document for each situation.

## Create an outline

An outline helps you organize information, ensures topics are covered, and helps you stay on-point. [How to create an outline](https://www.grammarly.com/blog/how-to-write-outline/).

* Add head tags (H1-H6) to each section. Make these directly linkable.
* Create a visual hierarchy. The most important information should be the highest contrast. The document should be easy to skim through. It should be easy to find what you're looking for.

## Ask good questions

### How to ask good questions

Most importantly,

* **Ask the right question.** You might want to treat a symptom or you might want to treat the underlying cause.
* **Ask the right people.** Don't ask a fish to climb a tree.
* **Ask at the right time.** Does this need to be asked now? Can we ask the question later? Is it important to get someone in the door before asking questions? Do we need to ask questions first so we know what door they want?
* **Ask in the right order.** Ask the most important items first and optional items last.

---

More specifically,

* **Name the document appropriately with details.** Make it easy to find. Make its purpose clear.
  * **Don't:** `Website requests`. *What are you requesting?*
  * **Do:** `Requesting webpage updates`.
* **Make it easy to consume.** The easier it is, the more likely they are to do it.
* **Explain why.** This helps communicate what you hope to get. It also alleviates any reservations they might have.
* **Give examples.** Don't just ask for an image, tell them what kind of images are useful and how the image needs to be prepared for use.
* **List do's and dont's.** This helps filter out unexpected or irrelevant data.
* **Provide instructions via calls to action** like `Insert answer here` and `list items below`.
* **Set their expectations.** A person might ask for the sun when you're only able to deliver the moon.
* **Limit your expectations.** The person writing an issue might not know how to use Photoshop.
* **Be strategically vague.** Example: "what do you hope to achieve" versus "what color do you want this to be?"
* **Be specific when it helps.** Example: "this issue is for bug reports" versus "this issue is for bug reports on codefresh.io excluding the community site."
* **Define important terms.** If you need them to understand something, explain it to them. Don't assume they are reading the same homonym. Don't make someone look up a definition. Don't assume familiarity.
* **Link to other documentation.** In order to keep the document short and relevant, don't include all information in the document. Example: "our team workflow is documented over there."
* **Demonstrate with preference.** Don't just say "take a screenshot," link them to your preferred article on how to do that. Don't just ask their browser version, link them to your preferred method to obtain that information. Sure they could google it, but the resource they find might not follow all of the steps you need.
* **Eliminate redundancies and combine similarities.** People don't like to give the same answer twice. They also may answer the same question differently. In order to facilitate data integrity, try to combine questions when possible.
* **Group similar topics by subject matter.** This allows them to skip sections that might not be applicable. It also allows you to navigate directly to sections you're interested in.

### What are good questions

#### The 5 W

[The 5 Ws](https://en.wikipedia.org/wiki/Five_Ws) are a basic tenet in information gathering and problem solving. Who, what, when, where, why, and sometimes how. Below are some example questions from each of the 5Ws.

**Data can be gathered in the positive or the negative (is / isn't, will / won't, etc).** It is preferable to only use positive, but sometimes it's necessary to use both.

Example 1: Brandon will be building X. ~~Brandon won't be building Y.~~ Chad will be building Y.

Example 2: Add feature X to section Y on all pages, *except for page Z*.

##### What

What do they want to achieve? What is the unexpected application behavior? What is the user unable to accomplish? What metrics allow us to measure that? What are the requirements?

##### Why

Why do they want to do it? Why is this a problem? Why does the user need to do this? Why is this important to the company?

##### Where

Where is this happening? Where do they expect it to happen? Where do we expect it to happen? Where are the users coming from? Where do they want to go?

##### Who

Who is the intended audience? Who does this bug impact? Who will be building this feature? Who should we ask for more information?

##### When

When is the right time in a [user journey](https://uxplanet.org/a-beginners-guide-to-user-journey-mapping-bd914f4c517c) for this to happen? When will this appear on the website and when will it stay until? When is this valid? When is the requested due date?

##### How

Don't just say what needs to be done, help them do it by listing steps and options.

# Peer review of templates

Before releasing a document, it's best practice to have it reviewed by both consumers and producers. Consumers will help you identify questions that need clarification. Producers will help you identify what's missing. Both will help you identify unnecessary items.

# Gather data and iterate

After you release a document, if readers are having trouble, it could be an indicator that:

* They don't want to do something. Rewrite the section in a way that alleviates their concerns.
* They don't understand the question. Try rephrasing it for clarity. Add definitions. Give examples.
* You're documenting the wrong thing. Try approaching it from another perspective.

With each change you make to a document, record why you made that change so you don't make the same mistake repeatedly. Check to see if the change you made in one section is applicable to other sections. Review the other documents to see if they can benefit from this update too.
