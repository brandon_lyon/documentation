
# Project Management

There is no right way to do project management. Everyone has different needs. The reason why nobody loves their project management software is because they're trying to adapt their workflow to a product instead of choosing the right product for their workflow.

## Suggestions

* Milestones should not be date based but rather project based.

## Traditional - One Size Fits All

* [Jira](https://www.atlassian.com/software/jira)
* [GitLab](https://about.gitlab.com/solutions/agile-delivery/)
* [GitHub](https://github.com/features/issues)

## Via OKRs

* [Tability](https://www.tability.io/)
* [Asana](https://asana.com/)

## Via Roadmap

* [Tahsk](https://tahsk.com/)

## Via Project Owner/Driver

* [Nifty](https://niftypm.com/portfolios)

## Via Customer Experience

* [Chisel](https://chisellabs.com/)

## Via Functional Spec

* [Zepl](https://zepel.io/)

## Via Groups

* [YouTrack](https://www.jetbrains.com/youtrack/)
* [Favro](https://www.favro.com/)

## Via Cross-Functional Launches

* [ClickUp](https://clickup.com/teams/project-management)

## Via Keep It Simple

* [Trello](https://trello.com/)

## Via Task Group

* [Hygger](https://hygger.io/)

## Via Everything is an Object in a Table

* [Monday](https://monday.com/)
* [Notion](https://notion.so/)

## Via Client Services - Billable Hours

* [Paymo](https://paymoapp.com/)

## Via Resource Capacity

* [Runrun](https://runrun.it/)

## Via Team Members

* [Yalla](https://yalla.team/)

## Via Automation, Personalization, and Alerts

* [Active Collab](https://activecollab.com/features)
* [Forecast](https://www.forecast.app/platform/automation)

## Via Work Request Forms

* [Asana](https://asana.com/)

## Via People Self-Reporting

* [Jostle](https://jostle.me/product/how/)

## Via Whiteboard

* [ConceptBoard](https://conceptboard.com/features/)

## Via Ops

* [Pipefy](https://www.pipefy.com/)

## Via Feature Release Cycles

* [TBD](#)

## Via Risk Analysis

* [TBD](#)

