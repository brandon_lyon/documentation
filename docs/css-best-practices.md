CSS is a visual language used to classify objects and define rules about how they should be displayed. CSS skills aren't restricted to developers anymore. There are benefits to having a basic understanding of the language, regardless of your occupation.

Figuring out where to start can be intimidating, but in CSS there are some easy rules to get started with. Below are some tips and tricks to help you get started with CSS and form good habits that can make your life easier in the long run. 

# Good Habits to Form when Working with CSS

## Learn the Layout Logic

CSS will lay things out in a sectioned grid organized from top-to-bottom, left-to-right. Certain style rules can break this grid. Picture the parts of a webpage as a grid and you can start to see how rules might be applied. You can use developer tools to see which rules target which objects. 

## Name Your Code as if it Were Human

Use multiple names (first name, middle name, last name). 

A single word is often not descriptive enough. Be specific and combine multiple words. Some examples are user-name, user-avatar, or user-location. Using multiple words minimizes conflicts from accidentally sharing names (for example all three of those could be named just "user", which would be bad).

Selectors should be a combination of approximately 3 elements when not using an ID. For example `.user-list .single-user .user-name {}` or `.user-list .single-user .user-avatar {}`. If you just selected .avatar, that could be an avatar in the header or it could be an avatar in the body. There could be more than one avatar in the body. It's good to have multiple frames of reference when choosing selectors. 

Avoid creating classnames based on attributes like color, size, or location. Button-blue could just as easily be named button-primary. The primary button’s color might later be changed to red, making your classname of button-blue inaccurate, forcing you to change the classname. Either way, it's the primary style of button. 

## Use the Correct Units

PX used to be great units of measurement before screens could have variable pixel densities. Now font sizes should be relative rather than absolute. Line-heights can be relative to the font-size, using EM. Margins and padding can use EM so they can scale with changes in font-size as viewports shrink. You may want to make certain items sized relative to the viewport dimensions. VW and VH are great new tools which one should take advantage of.

## Use a CSS Reset

Web browsers existed before CSS. There are default styles browsers start with, and each browser uses different ones. It's considered best practice to adopt a CSS reset in order to obtain desired outcomes when styling a website. My personal favorite is named "normalize.css". 

## Keep Your Code Organized

Come up with an organization scheme and stay consistent. Organizing your attributes alphabetically will make it easy to find duplicate sections of code. Organize your selectors by the order they appear on the page (top to bottom and left to right). Indent your code properly by using either tabs or spaces, but not both. 

## Make Your Code Efficient

Time is valuable. Make your website fast and efficient. Reduce the use of images, fonts, and star selectors. Avoid duplicate style declarations. Sprite your icons. Don't include CSS unless it's used on the specific page you're working on. Use loading animations and transitions. Don't animate something for 3 seconds when it only needs ⅓ of a second. 

## Design for Mobile First

Mobile devices have more constraints than a desktop computer. It's easier to start small and get large than it is to start large and get small. Hover states don't work on a touchscreen. Make sure your clickable items are easy to touch with a finger. 

## Don't be Afraid to Learn Javascript

Javascript, HTML, and CSS are intertwined. Yes you can build a website without JS and yes you should avoid using JS as much as possible. There are certain things which can't be done in a browser without JS. Javascript can be used to manipulate CSS in ways which normal stylesheets can't. After you've learned CSS you should learn javascript. 

# Tips for Getting Started with CSS

## The Longest Journey Starts with the Smallest Step

CSS is a large and complex topic. You need to start with a single small and focused goal in mind. For example, "I want to specify font sizes". Every journey (build a website) starts with a first step (make some text bigger). Your next step might be figuring out how to change the color of that text. 

## Find Examples

The internet is full of video tutorials, articles, and books on how to use CSS. Technical documentation won't help as much as examples of how real people solved the same problem as you. Lots of software on the internet is open source and programmers are often friendly folks who are enthusiastic about discussing and sharing techniques. 

## Don't be Afraid of Trial and Error

Unlike physical items, code is not a tangible resource. If you get something wrong you don't lose the cost of raw materials. You can't break a computer or website with CSS. Backup your work, make a few changes, save them, and check the page in a  browser. You can use the undo command if you broke something. If you really broke something then you can restore it from the backup. 

## Become Familiar with Developer Tools

You can use a web browser's developer tools to see how a website works. All of every website's front-end code is accessible for you to learn from by using a browser inspector. Developer tools are indispensable when working with CSS. They can help you find the source of your bugs and optimize code. You should learn as much about them as you can. 

## Join a Community and Ask Questions

Developers learn on the internet from a plethora of communities. Abstract topics like software have hundreds of different ways to accomplish the same task. Online communities will help you determine the pros and cons of different approaches.
