## What to expect

1. Once a request is submitted, it will go into our request backlog.
1. Every so often, we review our backlog to triage and refine requests. We do this to make sure they are actionable, can be delivered in a timely fashion, and meet expected requirements.
1. After a request has been refined, it is ready for work. Note this is NOT a promise to work on it.
1. Every so often, we review our ready queue in order to plan our quarterly themes.
1. If we have available capacity, your request is ready, and aligns with a theme or OKR, then we will place the request into our accepted queue.
1. Every agile sprint cycle, we review our available capacity and prioritize projects in our accepted queue.
1. Once your request has been assigned a working timeframe, this is when we plan to work on the project. If anything happens that would prevent us from working on it during that timeframe, we will communicate that ASAP.
1. When a request has been completed, the card will be marked as Live / Approved. Eventually, it will be [archived](https://help.trello.com/article/795-archiving-and-deleting-cards) and hidden from boards. Archived items are still searchable.
