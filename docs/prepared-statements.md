* INTRODUCTIONS
    * Thank you for filing an issue, we appreciate your contribution.
    * This item has been added to our request queue. We will review the feasibility and get back to you.
* DUPLICATE
    * This item is a duplicate of X. The current status of that is Y. We have added any new details to the preexisting issue.
* NOT ACTIONABLE
    * Our team does not appear to have enough actionable details at this time. Please review our [instructions](#) and our [project request template](#). We appear to be missing the following items: x, y, z.
    * If this item is not actionable by the next planning deadline, [insert date], then we will close the issue.
* COPY
    * This request does not appear to contain a sufficient copy document. Requests are not actionable without refined copy.
    * Please flesh out the following items: X, Y, Z.
* TIME
    * Given available resources and the requested scope, we are unlikely to complete delivery in the requested timeframe.
    * You appear to be requesting immediate assistance that is not scheduled for development this cycle. Please start a discussion in Slack so that we can align expectations for refining and prioritizing the request.
* PRIORITY
    * We would like to help, but your request appears to have a weaker business case than other projects. Regrettably we have to defer this project unless a stronger case is presented.
* OKRs
    * This request does not appear to fit our OKRs and we cannot guarantee a date at this time.
    * This item was not ready for this quarter’s planning deadline. We will move this to the deferred queue for consideration next quarter.
* SCOPE
    * Your latest request appears to be outside of the agreed upon scope. Please file a new request.
    * The scope of this request appears to be too large. Please prioritize the requested pieces so that we know which aspects are most important.
    * For this request, we are assisting by placing content into a preexisting design. Modifying or creating new templates is out of scope at this time.
* BLOCKED
    * This item is currently blocked by X. We cannot consider timeframes until blockers are resolved.
* OVERLOADED
    * Our workload is currently overflowing and we won’t be able to respond to this request within a week. If you need more timely support, please ask someone for help in the {{SLACK_CHANNEL_NAME}} Slack channel.
* TEMPLATES
    * This does not appear to match a known template for requesting resources from our team. We ask that you file a request using our [project request template](#). If you need assistance using the template please contact us in the {{SLACK_CHANNEL_NAME}} Slack channel.
* SNOWFLAKE (ONE-OFF) REQUESTS
    * This request does not appear to prioritize long-lasting templates, tools, or projects which enable self-service over one-off pages that change frequently. Can you please provide more information about why an exception might be necessary?
* STALE
    * This issue appears stale. Newer issues have been filed and/or the impacted locations have been updated since this issue was created. We're closing this issue for now but feel free to re-open it with fresh information using our template to [request a project](https://gitlab.com/brandon_lyon/documentation/-/blob/master/templates/request-a-project.md).
