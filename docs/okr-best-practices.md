# OKR Best Practices

1. Do not create OKRs that have external dependencies. Instead, say "we can't make that an OKR until x/y/z happens".
