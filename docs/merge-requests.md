This document is https://creativecommons.org/licenses/by/3.0/ by Brandon M Lyon

Merge requests are an important process where we peer review incoming changes to ensure code and content meets our standards and does not break any existing functionality.

In a continuous integration environment, people with merge permissions are the final check before content is released. Please use this privilege responsibly. Ensure that merge requests align with our [team and company values](#).

## How to review

* First review the description and discussions in the MR and any related issues.
* Identify the URLs and locations that will need to be tested.
* Review any documentation regarding testing procedures related to those locations. For example...
    * [Cookie consent](#).
    * [Lead generation forms](#).
* Look at the specific code changes in the MR to see how the site could be impacted by those changes.
* Check to see if any of the updated items are shared by other locations.
* Validate expected functionality per the documentation below.
* Ask codeowners or people who have previously updated that code or documentation for an additional reviews.
    * This information is usually found in the git history or sometimes in a codeowners file.
    * They are familiar with the item.
    * They might have different plans for how the item should be used.
    * They might know the history regarding why decisions were made when implementing that item originally.
    * They can assist you when assessing potential impacts.
    * They might know related codepaths, legalities, edge cases, or other things that you are unaware of.
* In addition to stakeholder and codeowner reviews, try to ask for a review from someone who represents an end-user.
    * Example: an SDR (sales) is familiar with how buyers use our website in general when looking for information.

## Always look for

1. Do the changes impact what they are supposed to impact as expected?
1. Do the changes impact something they are NOT supposed to impact?
1. Do all of the buttons and links function and go to an expected destination?
1. Does the sitemap generate as expected? Will this change impact it?
1. Are any deleted URLs redirecting appropriately?
1. Check all relevant browsers.
    * Relevant browsers are defined as those a significant percentage of our end-users utilize which have a different underlying engine.
        * Chrome (Blink), Firefox (Gecko), and Safari (Webkit).
        * If applicable to the target market/audience, Internet Explorer compatibility might also be required.
1. Check all relevant device resolutions.
    * If this is an email, social, or ad campaign it's likely going to be viewed first on a mobile device.
1. Are any resources are appropriately sized and compressed?
1. Are our [deverloper guidelines](#) being followed?
    * TODO: documentation link
1. Have appropriate stakeholders approved the change?
    * If this is a legal document, has legal signed off on it? If this is a partner program, has the liason approved it? Etc...

## Regression risks

### Overall

1. Does the cookie consent system (GDPR, CCPA, LGPD, etc) behave as expected?
    * Please review from a relevant geolocation via VPN and ensure related code behaves appropriately.
1. Are forms behaving as expected?
    * Before submitting the form?
    * While submitting the form?
    * After submitting the form?
1. Does search work as expected...
    * In the primary site navigation?
    * On the blog?
    * In other search buckets?
1 Are third party embeds such as youtube, twitter, surveys, and calendars, working?

### Javascript libraries

We use javascript extensively throughout the website. Before updating a library, please ensure it doesn't break key locations as outlined in the rest of the regression test section. Additionally, please check the following javascript-related areas:

* Analytics, tracking, and other marketing libraries.
* Contact, subscription, and lead generation forms.
* Site search.
* Cookie consent.
* Header navigation dropdowns and mobile menu.
* Shared sidebars.
* Charts and reports.

### Core business requirements

1. Do pages and flows related to core business aspects behave as expected?

## Code

1. Is the code well written?
    * Note that "well written" is relatively subjective but there are industry best practices such as [DRY code](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), efficient code, and secure code.
1. Does this code expose or implement any PII or other security related items?
1. Are any variables, constants, code parameters, and other items named appropriately?
1. Is the code organized logically?
1. Is code commented where appropriate?
1. Are functions appropriately sized? Are they small or monolithic?
1. Will or should this code impact any automated tests? If so, do they work as expected?
1. Are there any new javascript console errors or warnings introduced?
1. Please disable any console logs before merging.

## Copy

1. Are there any typos in the copy?
1. Are any dates correct? (examples: 'Tuesday the 31' but the 31 is a Wednesday, or the year is wrong, 41 is not a valid day of the month but 14 is...)
1. Are our [writing style guidelines](#) and brand tone of voice being followed?
    * TODO: documentation link

## Conversion

1. Does this follow best practices for conversion?
    * TODO: documentation link

## Design

1. Does this follow our [brand guidelines](#)?
    * TODO: documentation link
1. Do submitted images support our company values of [diversity, inclusion, and belonging](#)?
1. Are submitted images and other resources appropriately sized and compressed?
1. Are images sufficiently clear and not blurry?

## UX

1. Does this follow [best practices for UX](#)?
    * TODO: documentation link

## Accessibility

1. Does this follow [best practices for accessibilty](#)?
    * TODO: documentation link

## Social media

1. Does this follow our [social media guidelines](#)?
    * TODO: documentation link.
1. Are the elements necessary for social media present?
    * Opengraph image?
    * Appropriate title and description?

## SEO & analytics

1. Does this follow [best practices for SEO](#)?
    * TODO: documentation link
1. Has SEO analysis been done? Are the required elements present?
1. Will this change impact metrics in a meaningful way?
    * Are the required analytics being gathered as expected?
    * Are the teams impacted by these metrics aware of the upcoming changes?

## Experiments

1. Are the A/B tests behaving as expected?
    * On the control experience?
    * On the test experience?
    * Are the necessary analytics being gathered to measure impact of the test?

## Guideline links

* [Engineering's style guidelines](#).
    * [CSS guidelines](#).
    * [Javascript guidelines](#).
    * [Backend guidelines](#)
* [Engineering's code review handbook](#).
* [Marketing writing style guide](#).
* [Marketing naming conventions](#).
* [Marketing tone of voice](#)
* [Brand guidelines](#).
