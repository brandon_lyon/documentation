Goal: Create a communication plan for responsibility handoffs.

* {NAME1} has been designated as the primary responder for issues related to {PROJECT}.
* {NAME2} has been designated as a backup responder for issues related to {PROJECT}.
* From now on, issues related to {PROJECT} will go to {NAME1} for refinement. If {NAME1} is unavailable, {NAME2} will serve as backup.
* {PROJECT} is currently {inactive | active(issue) | on hold(reason & issue)}.
* Please familiarize yourself with {PROJECT} and prepare some questions to ask. Feel free to schedule a meeting if you want one.
* In order to familiarize yourself with {PROJECT}
    * Look at the primary page's git history to understand who has been working on it and what has changed over time.
        * Screencast of how to find the git history.
    * When reviewing the merge requests for that history, also review the linked issues so you get an understanding of why changes were made and who decided or provided content.
        * Screencast of where to find linked issues.
    * Search the handbook for related documentation.
