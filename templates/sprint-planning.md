
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

# {TEAM NAME} Sprint Planning

## Issue weighting tips

1. Under-commit to leave room for the unexpected. You can always pull in more issues later.
1. Leave a buffer to resolve tech debt.
1. Leave room for agency day.
1. Don’t forget to include PTO when calculating maximum point capacity per sprint.
1. Everyone has different maximum point capacities per sprint.

## Links

- [TRIAGE BOARD](#)
- [KANBAN BOARD](#)
- [QUARTERLY GOALS](#)

## Sprint starting {YYYY-MM-DD}

### Is anyone taking time off?

- {NAME}, {DATE START}, {DATE END}, {OPTIONAL REASON}
- Repeat for each team member taking PTO

### Discussion items

- {NAME}: Short description

### Unassigned but needed soon

- [REQUEST TITLE](#)
- Repeat for each item

### To do this sprint

- {NAME}:
    - {WEIGHT} [TITLE](#)
    - Repeat
- Repeat for each team member
