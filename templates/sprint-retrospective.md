
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

# {TEAM NAME} Sprint Retrospective

## Red yellow green

How did the sprint feel to you?

1. {INITIALS}: {COLOR} - {REASON}
1. Repeat for each team member

## Things that went well

1. {INITIALS}: description
    1. {INITIALS}: Discussion notes
    1. Repeat for each discussion note
1. Repeat for each team member with something to say

## Things to improve upon

1. {INITIALS}: description
    1. {INITIALS}: Discussion notes
    1. Repeat for each discussion note
1. Repeat for each team member with something to say

## Review progress

Review action items from previous sprint recaps in order to progress

## Action items

1. {INITIALS}: description
    1. {INITIALS}: Discussion notes
    1. Repeat for each discussion note
1. Repeat for each team member with something to say
