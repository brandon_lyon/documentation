**This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)**

---

This template is for filing a bug report related to a specific microsite property [insert URL here].

This is not the correct location for requests related to [insert comma separated list here of inapplicable microsite properties].

For more information please visit [insert documentation link here].

#### What is/are the relevant URL(s)

`Insert full URL(s) here`

#### Briefly describe the bug

Example: A link is broken and when hovering over it the color is the wrong color and poorly aligned.

`Insert answer here`

#### If possible, please specify how to reproduce the bug

`Insert answer here`

#### Please provide any relevant screenshots or screencasts

How to take a screenshot: https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/ or record a screen https://www.vidyard.com/blog/how-to-screen-record/

`Insert screenshot here`

#### What is your window size, browser, operating system, and versions

Please use the following website to determine your browser specifications and provide a link developers can follow:

What is my browser https://www.whatsmybrowser.org

`Insert results URL here`

#### What computing device are you using?

Please list the brand and model (Example: Samsung Galaxy S 10 Plus)

`Insert answer here`

#### Have you tried a fresh incognito window? Could this be related to cookies or account type/state?

- [ ] I tried a fresh incognito window & it had no impact.
- [ ] The problem goes away when using an incognito window.
- [ ] The problem only happens when certain cookies are set to a specific value (please specify below).

#### What is your geographic location

Why do we ask for this? Certain bugs may be geographically related. For example, there could be a problem with a content distribution network (CDN) node in your region. If you're in the European Union, it could be related to GDPR policy and cookies.

`Insert answer here`

#### What type of network are you connected to?

**Type**

- [ ] Wired
- [ ] Wifi
- [ ] Cellular (4G, 5G, etc)
- [ ] Satellite
- [ ] Other

`Insert details here if applicable`

**Location**

- [ ] Home
- [ ] Workplace
- [ ] Public venue
- [ ] Other

`Insert details here if applicable`
