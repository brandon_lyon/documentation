# Regression testing procedures template

## Supported Browsers & Devices

1. Because iOS blocks certain analytics, we don’t know exact numbers, but ~X% of unique visitors identify as unknown. Not all unknown visitors are iOS though.
1. Device analytics numbers differ between {tool name} and GoogleAnalytics. Because GA omits data it can’t measure (ie unknown), we favor {tool name} for device analytics.
1. Mobile visitors are likely to read email, visit landing pages, and consume notifications.
1. Desktop users are likely to consume more content and contribute more to acquisition, growth, & retention.

### Display Sizes

1. Overwhelmingly use 1920x1080 monitors.
1. The smallest sized non-mobile monitor with significant traffic is 1280x720.
1. Our smallest sized common mobile device appears to be 360x640.

### Browsers

{insert data here, it shifts regularly due to market trends}

1. Chrome 66%
1. Safari 8%
1. Edge 6%
1. Firefox 5%
1. Other 15%

## Operating Systems

{insert data here, this tends to depend on target audience}

1. Mac 38%
1. Windows 25%
1. Android 9%
1. iOS 6%
1. Linux 5%
1. Unknown 17%

### Devices

1. Desktop 70%
1. Mobile 13%
1. Unknown 17%

## Testing Locations

### Support

**We want people to keep paying us**

Loss of functionality risks losing existing customers.

1. Homepage
1. Log In
1. Support
1. Chat

### Signup

**We want people to start paying us**

Loss of functionality risks losing signups.

1. Sign Up
1. Pricing

### Contact

**We want people to talk to us**

Loss of functionality risks losing leads

1. Contact
1. Demo
1. Contact Sales
1. Enterprise

### Compliance

**We have legal obligations**

Loss of functionality risks legal penalties

1. Cookie compliance
1. Terms
1. Privacy
1. DPA (Data Processing Agreement)

### SEO

We want people to find us

Loss of functionality risks inbound traffic

1. Meta title
1. Meta description
1. Canonical URL
1. Lighthouse
1. Core Web Vitals
1. Does long content wrap to support max window width?

### Content Creation

**We want to stay relevant**

Loss of functionality risks ability to change

1. CMS (Sanity Studio)
1. Digital Asset Manager (Cloudinary)
1. Git (GitHub)
1. C.I.-C.D. (Vercel)
1. Forms (Hubspot)

### First Impressions

**We want people to trust us**

Broken or outdated content risks bounce rate

1. Blog posts
1. Landing pages
1. Gated content
    1. Before submission
    1. Error states
    1. After submission
1. Open Graph
    1. Image
    1. Title
    1. Description

### About the Company

**We want to generate interest in our company**

Loss of functionality risks hiring, press, and investors

1. About Us
1. Careers
1. Partners
1. Slide decks

### Information Architecture

**We want people to find what they need**

Loss of functionality risks bounce rate

1. News banner
1. Header site navigation
1. Footer site navigation
1. Search

### Digital Marketing Funnel

**We pay for people to find us**

Loss of functionality risks growth

1. UTM parameters
1. Tracking scripts (Hubspot, Facebook, Twitter, LinkedIn, etc)

## Product Information

**We want people to understand us.**

Insufficient information risks growth

1. Trust Center
1. Product (Platform) pages
1. Case studies
1. Solutions

## Topical Authority

**We want to build reputation as subject matter experts and industry leaders.**

Outdated, incorrect, or missing content risks inbound traffic

1. Topics > Article
1. Content Library > Guides

## Analytics & Split Testing

**We want to measure and improve.**

Lack of sufficient data risks metrics based decisions

1. Google Analytics
1. {other analytics}
1. A page with split testing enabled

## Content Localization

**We want our product to be accessible to global markets**

Lack of content localization risks growth, sales, and support in multiple markets.

1. Review the above locations, preferably with native speakers, in each applicable language, if the locations have corresponding localized content.

## Security

**We have legal obligations**

Loss of functionality risks legal penalties & reputation

1. Account security
    1. MFA
    1. Password requirements
    1. Login attempt limits
1. WAF
1. Patches & updates
1. Platform obfuscation
    1. Hide the login URL
    1. Hide any platform identifiers or version numbers