
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

## Goals

1. {GOAL} {OPTIONAL ATTACHED METRIC}
1. Repeat for each goal

## Proposed methodology

How and why we suggest breaking this into phases

## Stakeholders

- 

## Deliverables

### Phase 1

- [ ] {DESCRIPTION}
- Repeat for each deliverable

## Repeat for each phase

- [ ] {DESCRIPTION}
- Repeat for each deliverable

## Risks

- [ ] Checklist so we can mark that we have evaluated and mitigated the risks
- Repeat for each risk
