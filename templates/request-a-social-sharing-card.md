This is a request for an opengraph card. This preview is seen when sharing a link on platforms such as LinkedIn, Facebook, and Twitter, as seen in the screenshot below. This is especially important when developing a campaign that is designed to be shared, such as advertising or social activity like courses.

## General recommendations

1. **Try out and preview your copy using [metatags.io](https://metatags.io/)**
1. Value propositions are important. Please try to include a compelling reason why people should click on the link. Examples include but are not limited to: save time, get to market quicker, produce more, iterate faster, improve efficiency, advance your career, save money, build reputation, etc.

## Title - 60 characters max

*The title should be compelling, it is the boldest part. On a mobile device, this might be the only visible text. Titles should not contain advertising information, branding, or site name. Those may be present on other parts of the card.*

> Insert here what you recommend for the title.

## Description - 200 characters max

*Describe what the linked page is about AND why someone should care. The most important information should be at the start of the description. It might become truncated or people might stop reading.*

> Insert here what you recommend for the description.

## Image

*We will generate an image for you, but appreciate any suggestions you might have. Screenshots might be helpful.*

> Insert here any suggestions for imagery. If you have attached any screenshots, please mention that here.
