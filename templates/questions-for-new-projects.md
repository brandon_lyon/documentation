##### This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/).

# Introduction

#### This is not a comprehensive list and will be iterated upon.

# Landing page questions

Note that as a fullstack engineer, one is often responsible for ALL of these roles. This list is categorized in case there are teams supporting the roles.

## As a project manager

- **Where would this project happen?**
    - Please provide bulleted list containing any URLs that we'll be updating or creating.
    - Example: `https://domain.url/page/`
- **What does the request include?**
    - Example: Add topical content to the primary site navigation.
- **What does the request exclude?**
    - Example: Subtopics should not be included in this navigation update. They're linked to from the primary navigation.
- **Why would we do this?**
    - Please include both the primary business goal and the end-user impact. This usually includes the problem we're trying to solve.
    - Example: In order to improve long-tail conversions, we want to make it easier for end-users to find topical content.
    - If desired, you may list secondary, tertiary, and other goals. We will be focused on achieving the primary goal, but appreciate understanding how to assist other goals.
- **Please provide a list of any relevant resources.**
    - At a minimum, this should contain a copy doc. Other examples include annotated screenshots or screencasts, mockups of your request, user journeys, diagrams, spreadsheets, presentations, images, or design inspiration.
- **What is the primary KPI (key performance indicator)?**
    - Example: The primary KPI is webinar registrations.
    - If desired, you may designate secondary, tertiary, and other KPI.
- **Will this be gated content?**
- **When would you like this done?**
    - Note this question is different than "when is it due." Delivery dates are not guaranteed.
    - If your request has multiple delivery dates, please create a separate issue for each date.
    - Dates without reason are difficult to prioritize. Without enough lead time we will be unable to meet your requested date.
    - Example: Our requested date is X. We want to launch this page before a press release on X for a trade show that happens on Y. I have submitted this over one quarter in advance with the hope that it can be prioritized as a quarterly OKR.
        - [ ] No date requested.
        - [ ] YYYY-MM-DD-HH, insert reason.
- **Please provide a list of people related to the project AND why you listed them.**
    - Example: @ user1 as responsible for engineering, @ user2 as responsible for UX, @ user3 for approval, @ user4 as backup for @ user2, @ user5 for analytics and SEO, @ user6 for copy direction, @ user7 for legal department awareness.

## Event specific questions

- Please outline the schedule to the best of your current understanding.
    - Example:
        - MVC soft launch is 2021-06-11
        - Call for proposals is announced 2021-06-11
        - Location is announced 2021-07-11
        - Final speakers list is announced 2021-08-11
        - Travel & accommodation partnerships are announced 2021-08-11
        - Registration opens 2021-08-11
        - Final event schedule is listed 2021-10-11
        - The first day of the event is on 2021-11-11
        - The last day of the event is on 2021-11-16

## Webinar specific questions

- What happens after the webinar is finished?
    - Will this be recorded and uploaded somewhere for later viewing?
        - As gated content?
    - Will the webinar event be run again at a later date?
    - Will the page be redirected?

# As an analyst

- What data are we looking to measure?
- Where are we storing related data?
- How are we storing related data?
- What data are we comparing against?
- What timeframes are relevant?

## As a search marketer (SEO)

- Is the content relevant to the target keywords?
- Is the URL human-readable and applicable to the content or group?

## As a copy writer / reviewer

- Does this copy follow our brand guide?
- Does this copy pass the DIB (diversity, inclusion, belonging) checklist?
- Does this copy follow conversion best practices?
- Does this copy follow UX best practices?
- Does this copy follow engineering specifications?
- Is the copy an appropriate length...
    - From the visitor's point of view (don't make me think)
    - From the UX point of view (how do we help the visitor achieve their goals)
    - From a conversion point of view (how can we achieve our business goals)
    - From an engineering point of view (does this copy fit within technical specs)


## As a UX or IA

- What type of visitors are we targeting?
- What awareness stage is the visitor in?
- What part of the buyer's journey is this for?
- How do people get to this page?
- Why are they on this page?
- What are they trying to do on this page?
- What do we want them to do on this page?
- Where do they want to go after this page?
- Where do we want them to go after this page?
- What related content would help them decide?
- How can we build trust?
- How can we show reputation?
- Who or what are we comparing to?

# As a UI

- How can we add surprise and delight?
- Does this match brand guidelines?
- Have we accounted for microinteractions?
- Have we accounted for skeleton UI?
- What transitions and animations are required?
- Is this legible on required devices?
- Is this interact-able on required devices (ie finger-friendly on touchscreens)?

# As an ops

- Does this request include any tools?
- Do we already use those tools or are the new?
- What are we trying to achieve with it?
- Why would we choose this tool over other options?
- Who is this tool for?
- Who will be responsible for tool configuration? The vendor, other team members, or ops?
- How would this tool impact financials? (This helps during cost estimation)

# As security

- Could this impact application security?
- How can we mitigate risk?
- Is the risk acceptable?

# As legal

- Is this legal?
- What changes need to be made, if any, to the request?
- Does this require updating the privacy policy or terms?
- Is cookie consent accounted for?

## As an engineer

- If you require engineering to create or modify a template, please provide a link to the final prototype.
- If you require engineering to input content, please provide a link to the final copy doc.
- If you require engineering to input opengraph (social) information, please provide the [required content](https://ahrefs.com/blog/open-graph-meta-tags/).
- If you require engineering to input SEO information, please provide [META information](https://ahrefs.com/blog/seo-meta-tags/) and specify any relevant [schema](https://moz.com/learn/seo/schema-structured-data).

## As an email marketer

- Is this request for a drip campaign, a reusable template, or a one-off email send?
- Please provide a suggested copy doc and any related image assets.
    - Has this copy been reviewed for anti-spam best practices?
    - Please specify desired tokens in the copy doc.
        - Example: hello $username
- What user segment are we emailing?
    - How many people are in that segment?
- When would we like to start processing the email send?
- When would we like to finish processing the email send?

## As a digital marketer

- What platforms would we like to engage?
- What is the budget? Are we targeting a particular CPM?
- What audience are we targeting?
- What is the URL of the landing page we are targeting?
- How will we track campaign performance?
    - If UTM or other parameters, please specify a suggested list.
- How might we entice the audience?
- Please provide suggested copy.

## As a QA

- What does it look like it should do?
- What does it actually do?
- What is it supposed to do?
- Are all states accounted for (blank, active, focus, hover)?
- Has this introduced any regressions?
- Is this as expected in dev, test, and production environments before and after release?
- Does this work on expected devices at expected viewports (pointer, touch, voice, whatever).
- How does this perform outside of expected devices at unexpected viewports?
- Are all parts of the funnel accounted for and tested? Ie the things leading up to the landing page and following it, not just the landing page itself.
- Either now, in the future, or in the past... could this change have negative repercussions across SEO, accessibility, DIB, brand, community, income, expenditures, legal, CS, docs, user-generated content, or the ability for visitors to do what they're attempting to do?

## As an automation engineer

- Is there any existing automation that needs to be updated?
- Does this require new automation?
- What would we expect the automation to do?
- What are the structured data expectations?
- What is required? What is optional?
- Should this automation prevent, warn, or other?

## Redirect requests

- What pages do you want to redirect?
- Where do you want the pages to redirect to?
- What type of redirects are these? Permanent, temporary, or other?
- Don't forget to cleanup content we control and point it to the new URLs in order to maintain SEO quality.
