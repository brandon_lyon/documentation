
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

# {TEAM NAME} Sprint Planning

## Links

- [TRIAGE BOARD](#)
- [KANBAN BOARD](#)
- [QUARTERLY GOALS](#)

##Sprint ending on {date}

- {INITIALS}: [LINK TO RELEASED CONTENT](#) [LINK TO ISSUE OR MERGE REQUEST](#) [OPTIONAL LINK TO SCREENCAST](#)
1. Repeat for each delivered item
