**This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)**

---

This template is for requesting work related to a specific microsite property [insert URL here](#).

This is not the correct location for requests related to [insert comma separated list here of inapplicable microsite properties](#).

For more information please visit [insert documentation link here](#).

## Where might this project happen?

Please list the URLs or other deliverables we'll be working on.

## What are we requesting?

Please describe the request at a high level.

*Example 1: Improve the site navigation using the results from a card sorting exercise. Example 2: Add a new case study to our existing collection.*

## Help us build the business case

Describe why we would do this. Try to include the business need and the visitor need. This is often described as a problem we are trying to solve.

*Example 1: In order to improve feature adoption we want to make it easier for visitors to find feature lists. Increasing feature adoption decreases churn rate.*

*Example 2: Enterprise customers with large seat counts will not sign up without feature X.*

*Example 3: Implementing feature Y would be an important differentiator, decreasing competitor advantage.*

## What is the content?

Please share a link to a content document, change list, or mockup that contains information from our [content brainstorming template](https://gitlab.com/brandon_lyon/documentation/-/blob/master/templates/copy-brainstorming.md).

**Projects cannot be scheduled until we have copy.** 

Provided copy does not have to be final, but it does have to be releasable as-is. We can iterate afterward as needed.

## Who is involved?

Insert a list here of people related to the project **AND** why you listed them.

*Example: @ username for direction, @ username for approval, @ username for integrations, @ username for legal department awareness*.

## When would you like it done?

**If your request has multiple delivery dates, please create a separate issue for each deadline and link them to each other as blockers.**

*Dates without reason are difficult to prioritize. Without enough lead-time we will be unable to meet your deadline. Example: Our due date is X. We want to launch this page before a press release on Y for a trade show that happens on Z.*

Please answer in the following format:

*Year, month, day, hour (if needed), time zone (if needed)*

`YYYY-MM-DD-HH, Pacific, insert reason here`
