**This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)**

---

This template is for brainstorming copy.

#### Research

List any related pages belonging to competitors or alternative technologies that can be used for research.

1. Inspirations
1. Competitors
1. Alternatives

#### Audiences

1. Should this page talk to specific audiences? If so, then who?
    1. *Examples include but are not limited to: job roles (engineer, manager, dept head), account size (large, med, small), account age (prospects, young accounts, old accounts), account type (private sector, public sector, community), geographic location (AMER, EMEA, APAC), event audiences (attendees, speakers, partners, press) investors (prospects, preexisting)*
1. Is this related to industry verticals?

#### Document the Objectives

1. Introduce this offering, its differentiating factors, and its role within the wider portfolio.
1. Inspire enthusiasm and creative thought about how one might use the product to solve pressing business problems at their company.
1. Serve as the canonical page for this product’s discovery.

#### Visitor Outcomes

1. Describe at least 3 benefits that will be realized by adopting the product. These will be featured prominently on the page, so include as much detail as possible with an emphasis on differentiation.
1. How will a customer feel after realizing the outcomes listed above?

#### Product Features

1. List at least 3 differentiating features or capabilities that should be mentioned on this page.

#### Use Cases

1. List any applicable use cases.

#### Testimonial

1. Reference 1 customer testimonial to include on this page.

#### Case Study

1. Reference 1 case study that pertains to this product.

#### Partners

1. Reference any partners whose logo can be displayed on this page.

#### Developer Resources

1. List any sample apps, blueprints, or technical guides that could be included with this page.

#### SEO & Social Sharing Information

1. In 60 characters or less, please specify a title for search engines and social sharing.
1. In 160 characters or less, please specify a description for search engines and social sharing.

#### What is the primary call to action?

What button should the user click?

*Examples: Free trial, get ebook, install, upgrade...*