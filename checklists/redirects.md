This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

This checklist is for implementing URL redirects.

- [ ] Relevant pages have been moved.
- [ ] The pages still function as intended after being moved.
- [ ] Relevant canonical URLs have been updated (if any).
- [ ] Relevant automations have been updated (if any).
- [ ] Tag manager, analytics, and other items specifying this URL have been updated (if any).
- [ ] There are no A/B or other split tests running on this page.
- [ ] Integrations such as localization and cookie consent are working as expected.
- [ ] UTMs and other URL parameters have been taken into account.
- [ ] Redirect rules have been created.
- [ ] Monitoring is in place for potential SEO, UX, or CX impacts.
- [ ] The redirect works as intended in production (don't forget to verify after release).