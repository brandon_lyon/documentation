This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

## Before the audit

- [ ] Present why, what, and how you'll be auditing
- [ ] Share the approach with stakeholders and SME subject matter experts for review
- [ ] Update your plan based on feedback

## Things to audit

- [ ] Backup strategy
    - [ ] Verify integrity of backups
    - [ ] Verify schedule of backups
    - [ ] Verify restoration procedures
- [ ] Integrations
- [ ] Logins
- [ ] Tokens & secrets
- [ ] Domains & certificates
- [ ] Expirations
- [ ] Application users
- [ ] Plugins
- [ ] Infrastructure security
    - [ ] Operating system & container updates
- [ ] Application security
    - [ ] Library and version updates
- [ ] Visitor-generated content security
    - [ ] Social, comments, & moderation
    - [ ] Captcha & Twilio
- [ ] Backend performance
    - [ ] Datadog or similar monitoring
    - [ ] Uptime & site status indicators
- [ ] Infrastructure performance
- [ ] Frontend performance
    - [ ] Lighthouse
    - [ ] File sizes & quantities
    - [ ] Paint times
    - [ ] Asset optimization (images, video) via CDN or DAM such as Cloudinary
    - [ ] LogRocket or similar monitoring
- [ ] Templates
- [ ] SEO performance
- [ ] Automation
- [ ] Localization strategy
- [ ] Where do we want to go
- [ ] What do we need to get there

## After the audit

- [ ] Document what we learn
- [ ] Create a retrospective
- [ ] Establish best practices moving forward
- [ ] Share the above items
