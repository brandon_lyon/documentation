
## How much resistance do you want when pushing down a key?

- [ ] MORE
- [ ] Average
- [ ] less

## How loud do you want your keyboard

- [ ] LOUD 
- [ ] Average
- [ ] quiet

## How far do you want to push the key?

- [ ] FAR
- [ ] Average
- [ ] short

## How do you want it to feel when the key has hit bottom?

- [ ] HARD
- [ ] Average
- [ ] soft

## How do you want the action to feel?

- [ ] I want to feel more resistance the farther down I've pushed the key.
- [ ] I want the action to feel the same the whole time.
- [ ] I want a solid feeling click that lets me know I have pushed the key.
