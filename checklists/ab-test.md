
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

This checklist is based on the {TEAM NAME}(#) team's documentation for [ENGINEERING AB TESTS](#) using feature flag systems.

## Feature-flag-based A/B Testing

### Dev Environment

- [ ] A feature flag has been created.
- [ ] Appropriate experiment events have been attached to the flag.
- [ ] Your config has been adjusted to use the dev environment.
- [ ] The control variant appears as expected.
- [ ] The test variant appears as expected.

### Test Environment

- [ ] Your partial config has been adjusted to use the test environment.
- [ ] The javascript SDK has been enabled in the provider's test environment.
- [ ] The control variant has been reviewed in the provider's test environment without using preview URL parameters on a review app before release.
- [ ] The test variant has been reviewed in the provider's test environment without using preview URL parameters on a review app before release.
- [ ] All critical functionality has been reviewed.
- [ ] All of the buttons work as expected, outside and inside the popup.
- [ ] All of the classnames and IDs required for tracking are present.
- [ ] The feature flag is enabled in the provider's test environment.
- [ ] The metrics are set to record in the provider's test environment.
- [ ] The metrics are recording as expected in the provider's test environment.
- [ ] The provider's debugger has been used to verify that the metrics are being recorded.
- [ ] This has been cross-browser tested in Chrome, Firefox, and Safari in desktop and mobile viewports with at least one mobile touchscreen device [in meatspace](https://www.urbandictionary.com/define.php?term=meatspace).

### Production Environment

- [ ] Your partial config has been adjusted to use the production environment, committed, and pushed.
- [ ] You have merged the branch into master. Note that after the pipeline is deployed it will take additional time for the CDN to propogate.
- [ ] The control variant has been verified by using URL preview parameters on production after release.
- [ ] The test variant has been verified by using URL preview parameters on production after release.
- [ ] The rollout rule is setup correctly in the provider's production environment.
- [ ] The feature flag is enabled in the provider's production environment.
- [ ] The metrics are set to record in the provider's production environment.
- [ ] The feature flag javascript SDK has been enabled. This is the final step and live experiment data will now be gathered.

### Monitoring Phase

- [ ] I have created and linked an issue for monitoring the results of the test, including frequent check-ins about how many monthly allocated MAU and metrics we're using, until statistical significance is gathered. For production pricing pages with a 50/50 rollout, this is likely at least 2 weeks.
- [ ] I will be closely watching to ensure that production metrics aren't significantly negatively impacted and might end the experiment early if that is the case.

### Closing Phase

- [ ] I have created and linked a retrospective issue and reserved time to document the results and what we learned from this experiment once it's over. The retrospective should also recommend next steps for future test proposals.
- [ ] I have disabled the feature flag in the provider's production, test, and dev environments.
- [ ] I have disabled the metrics gathering for the feature flag in the provider's production, test, and dev environments. This normally happens when disabling the feature flag.
- [ ] I have disabled the javascript SDK for the feature flag in the provider's production, test, and dev environments. This will prevent phantom MAU accumulation.

### Notes

- If you disable the feature flags but not the SDK, then that feature flags will no longer be served, BUT anyone with a browser tab open may continue to submit data. People have been known to submit experiment data weeks after the test has ended. This is why we recommend disabling the javascript sdk once the experiment has ended.
