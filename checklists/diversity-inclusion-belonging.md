
This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

This checklist is based on the {TEAM NAME}(#) team's documentation for [DIVERSITY, INCLUSION, AND BELONGING](#).

# Diversity, Inclusion, and Belonging

## Are the images inclusive?

- [ ] Try to keep images neutral in terms of gender, ethnicity, etc.
- [ ] There is alt text for SEO and accessibility.

## Is the writing inclusive?

- [ ] This content uses inclusive language.
    - [ ] Not aggressive.
    - [ ] Avoids negative gender connotations.
    - [ ] Avoids military analogies.
- [ ] Uses ally language and does not disparage others.
- [ ] This content uses people's preferred pronouns.
