**This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)**

---

# [Live survey](https://va9tjw68ogi.typeform.com/to/GX7jaeYx)

> **A live version of this survey is available to fill out at https://va9tjw68ogi.typeform.com/to/GX7jaeYx**

#### Instructions

This template is for gathering personal preference information to facilitate working with coworkers.

Please choose only one item from each checklist, unless directed otherwise.

If you would like to suggest any changes, please reach out to Brandon Lyon. He is very open to this.

#### How do you prefer to communicate?

- [ ] Ticketing systems like Jira, Trello, GitLab, etc
- [ ] Text chat systems like Slack
- [ ] Video systems like Zoom
- [ ] Email
- [ ] No preference
- [ ] Other

#### When do you like to collaborate?

These preferences refer to your local time zone. I prefer...

- [ ] Early in the day
- [ ] Middle of the day
- [ ] Late in the day
- [ ] No preference
- [ ] Other

#### My learning style

I tend to learn best via...

- [ ] Reading
    - Reading is key to understanding. Visuals or audio can be distracting or I might not process them as easily.
- [ ] Audio
    - Hearing and/or conversing is key to understanding. Visuals can be distracting or I might not process them as easily.
- [ ] Visual
    - Seeing something happen is key to understanding. Watching someone do something, often as a video, seeing things happen, learning by example.
- [ ] Kinesthetic
    - Movement is key to understanding. Learn by doing. Information is better retained when I am moving the mouse, clicking, etc. This is similar to visual except more interactive.

#### My social style

I consider myself an...

- [ ] Introvert
- [ ] Extrovert
- [ ] Ambivert

#### Feedback

Choose as many as you like from the following options. I prefer feedback that is...

- [ ] Direct
- [ ] Instant
- [ ] Gentle
- [ ] Frequent
- [ ] I don't like feedback
- [ ] No preference
- [ ] Other
