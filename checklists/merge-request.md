This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)

---

This checklist is based on the {TEAM NAME}(#) team's documentation for [REVIEWING MERGE REQUESTS](#).

## Required

- [ ]  **You have searched for any pages using shared files to ensure those pages won't be negatively impacted by changing those files (ie regression tests).**
- [ ]  Contents of these changes align with our [company values](#).
- [ ]  Contents of these changes have been reviewed for diversity, equity, inclusion, and belonging.
- [ ]  These changes have not decreased page speed significantly.
- [ ]  These changes try to maintain the single-source-of-truth principle SSOT.
- [ ]  These changes have been reviewed for accessibility best practices.
- [ ]  Relevant owners, subject matter experts, and stakeholders have reviewed the changes.
- [ ]  All interaction states have been tested such as active, focus, and hover.
- [ ]  All buttons and links go to where they're supposed to go.
- [ ]  Social media opengraph images have been added, with guidance from the relevant teams.
- [ ]  All images, PDFs, and other files have been optimized and follow our [image guidelines](#).
- [ ]  These changes behave as expected in all major browser rendering engines (Chrome, Firefox, and Safari).
- [ ]  These changes behave as expected on phone, tablet, laptop, and desktop screen sizes.
- [ ]  These changes work on a touchscreen. 1. Important information isn't contained solely within hover events. 2. Buttons are finger-friendly.
- [ ]  These changes work with our cookie consent platform.
- [ ]  All console logs have been disabled before releasing to production and there are no javascript errors on the page.
- [ ]  I reviewed these changes, and unrelated items were not accidentally included.

## Suggested

- [ ]  I have taken before and after screenshots of this project and attached them to the relevant ticket.
- [ ]  All items necessary to measure conversion impact have been accounted for.
- [ ]  UX research has been done that supports these changes.
- [ ]  These changes have been through SEO review.
- [ ]  These changes have been reviewed for security best practices.
- [ ]  If applicable, automation has been updated or created and operates as expected.
- [ ]  All blank states or empty lists are accounted for.