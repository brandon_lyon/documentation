JavaScript can be used in a browser to change parts of a webpage dynamically. It begins with the following pattern: If condition W is true, do thing X to target Y after event Z. Below are some tips and tricks to help you get started with JavaScript and form good habits that can make your life easier in the long run. (Note: examples in this article use jquery for simplicity.) 

## Start with Descriptions

Start by writing comments which describe in plain language what you're trying to do. Outline where you will need variables, functions, and events. You can always add more complexity like loops and data later on. For example after document ready, whenever someone clicks on a specific button, increment a counter on the page. 

## Learn Events Other than Document Ready

Half of JavaScript is knowing *what* you can do, while the other half is knowing *when* you can do it. 

Document ready is not the only trigger. It's very common to re-run the same function on document.ready, ajaxComplete, and window.resize. Modern JavaScript frameworks may use something like componentReady instead of documentReady. You can also run functions on scroll, click, touch, change, etc.

## Learn How to Use Parameters

You will want a technique for running the same function with different options. Parameters facilitate reuse by allowing variables to be passed into a function.

## Make All Functions Reuseable

Don't write events inside of functions. If the function is separate from the events, you can call that function any time. Instead of saying function X = make some button blue on click, you could say function X = make some button blue, function Y = on click run function X, and function Z = on scroll run function X. 

## Name Things Explicitly

Keep it straightforward. When naming something, base it on the type of object. If the object is a variable then the name should start with a noun and be followed by an adjective (ex: buttonPrimary). If the object is a function then it should start with a verb, followed by a noun (ex: disableButton). If the object is a target then it should start with the word target (ex: targetButton). 

Functions with the purpose of running groups of functions should start with a word like "run". 

```javascript
targetButton = $('#buttonPrimary'); 

   targetBackgroundColor = '#000'; 

   changeButtonBackgroundColor = function(targetButton, targetBackgroundColor) {}; 

   animateButton = function(targetButton); 

   runButtonActions = function() { 

   changeButtonBackgroundColor(); 

   animateButton(); 

} 

$(window.document).ajaxComplete(function() {runButtonActions();} 

$(window).resize(function() {runButtonActions();}
```

## Check if Things Exist Before Manipulating Them

A common JavaScript error is trying to do something to an element which doesn't exist. This is easily preventable by always confirming that something exists before you try to modify it. 

`if ($('#targetThing'')[0]){//do something because it exists};`

## Use Single Quotes Outside

JavaScript allows you to use single quotes and double quotes in the same statement. It will know what you mean as long as they are paired accordingly. That said, get into the habit of using single quotes as the outermost mark for anything which isn't a string, HTML string, or JSON. 

```javascript
alert('Say "Hello"'); 

alert("A string that's double quoted and contains a single quote"); 

alert('&lt;div id="thing"&gt;'); 

var jsonObject = '{"foo":"bar"}';
```

## Console Log Everything

JavaScript troubleshooting is not straightforward. You should start by adding a console.log for all parts of a troublesome function. Logging makes it possible to differentiate between expected and observed behaviors. Maybe your setter isn't working. Maybe your result is a string instead of a number. Maybe the element you're targeting isn't on the page. Without console logging it will be hard to track down what's wrong. 

## Adopt a Styleguide

Styleguides encourage good habits when writing code. Many large companies such as Google and Airbnb make their styleguides public online. It's in your best interest to learn from their examples. 

## Beyond the Basics

Once you've mastered the basics of JavaScript without a framework like jquery, you can move on to using server-side JavaScript via tools like nodejs, or database JavaScript via tools like mongodb. These days there are even tools to build mobile, desktop, wearable, and IoT applications using JavaScript. 
