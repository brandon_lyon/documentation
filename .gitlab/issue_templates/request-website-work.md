**This document is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by [Brandon M Lyon](https://about.brandonmlyon.com/)**

---

This template is for requesting work related to a specific microsite property [insert URL here](#).

This is not the correct location for requests related to [insert comma separated list here of inapplicable microsite properties](#).

For more information please visit [insert documentation link here](#).

## Before creating an issue

- [ ] I have reviewed the [setting yourself up for success](#) section of the handbook.
- [ ] I acknowledge that any content updates should also update [social media content](#) such as [images](#), titles, and descriptions on relevant pages. Not doing this can negatively impact related campaigns.

## Where would this project happen?

#### Insert here a bulleted list containing any URLs that we'll be updating or creating:

- https://domain.url/page/

## What does the request INCLUDE?

#### Insert the request here:

*Example: Improve the site navigation using the results from a card sorting exercise.*

#### Insert here a bulleted list containing links to any relevant resources:

At a minimum, this should contain a copy doc. Other examples include screenshots or screencasts, mockups of your request, user journeys, diagrams, spreadsheets, presentations, images, or design inspiration:

- Content document link

## What does the request EXCLUDE?

#### Insert here any items that are out of scope for this iteration:

*Example: Because we need to get this out quickly, we will not be adding new styles, just updating the content.*

## Why would we do this?

You should include both the business goal and the end-user impact. This usually includes the problem we're trying to solve.

You might find the following optional format useful: As a (insert role), I would like (insert request) so that (reason).

#### Insert here why we want to do this:

*Example: In order to improve long-tail conversions, we want to make it easier for end-users to find topical content. Alternate example: In order to improve stage adoption we want to make it easier for users to find feature lists.*

## Who would be involved?

If you are requesting a deliverable from someone, please create a separate issue with a due date, assign it to that individual, and link it to this issue as a blocker.

Please use the R.A.C.I. framework. This includes RESPONSIBLE individuals who are discipline DRIs, a single person ACCOUNTABLE for the project (usually the requestor), a backup for when they accountable is unavailable, the CONSULTED who have knowledge and feedback to consider but not necessarily implement, and stakeholders who should be INFORMED about the project.

#### Insert here a list of people related to the project AND why you listed them.

*Example: @ brandon_lyon as responsible for engineering, @ jhalloran as responsible for UX, @ tbarr for approval, @ dmor as backup for @ tbarr, @ shanerice for analytics and SEO, @ awithers for copy direction, @ lsayers for legal department awareness*.

## When would you like it done?

If your request has multiple delivery dates, please create an epic with a separate issue for each deadline.

Dates without reason are difficult to prioritize. Without enough lead time we will be unable to meet your deadline.

#### Insert here your requested date AND why:

*Example: Our requested date is X. We want to launch this page before a press release on X for a trade show that happens on Y. I have submitted this over a quarter in advance with the hope that it can be prioritized as a quarterly OKR.*

- [ ] No date requested.
- [ ] YYYY-MM-DD-HH, insert reason.
